﻿using System;

namespace ExampleAttributes
{
    static class Program
    {
        static void Main(string[] args)
        {

            string header = "campo1\tcampo2\tcampo3\tlon\tcampo4\tpeso\tcolor\t";
            string linea1 = "valor1\tvalor2\tvalor3\teste valor\tvalor4\totro valor distinto\totra propiedad\t";
            
            MyClass objeto1 = MyClass.CreateNewInstance(header.Split("\t"), linea1);
            System.Diagnostics.Debug.WriteLine(objeto1.Latitud);
            System.Diagnostics.Debug.WriteLine(objeto1.Longitud);
            System.Diagnostics.Debug.WriteLine(objeto1.Peso);
            System.Diagnostics.Debug.WriteLine(objeto1.Color);


            MyClass objeto2 = new MyClass();
            MyHelperStuff.InitInstance(objeto2, header.Split("\t"), linea1);

            System.Diagnostics.Debug.WriteLine(objeto2.Latitud);
            System.Diagnostics.Debug.WriteLine(objeto2.Longitud);
            System.Diagnostics.Debug.WriteLine(objeto2.Peso);
            System.Diagnostics.Debug.WriteLine(objeto2.Color);


        }
    }
}
