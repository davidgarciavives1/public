﻿using System;
using System.Collections.Generic;
using System.Text;


class MyClass
{

    [HeaderName("lon")]
    public string Longitud { get; set; }

    [HeaderName("lat")]
    public string Latitud{ get; set; }

    [HeaderName("peso")]
    public string Peso { get; set; }

    [HeaderName("color")]
    public string Color{ get; set; }


    public static MyClass CreateNewInstance(string[] headers, string lineValue)
    {

        MyClass newObject = new MyClass();

        Type t = newObject.GetType();

        foreach (var p in t.GetProperties())
        {
            var attr = (HeaderNameAttribute) System.Attribute.GetCustomAttribute(p, typeof(HeaderNameAttribute));
            if( attr?.fieldName != "" )
            {
                int index = Array.IndexOf(headers, attr.fieldName);

                if (index > -1 )
                {
                    p.SetValue(newObject, lineValue.Split("\t")[index]);
                }
            }
        }

        return newObject;
    }


    public static void InitInstance(object emtpyObject,  string[] headers, string lineValue)
    {
        Type t = emtpyObject.GetType();

        foreach (var p in t.GetProperties())
        {
            var attr = (HeaderNameAttribute)System.Attribute.GetCustomAttribute(p, typeof(HeaderNameAttribute));

            if (attr?.fieldName != "")
            {
                int index = Array.IndexOf(headers, attr.fieldName);

                if (index > -1)
                {
                    p.SetValue(emtpyObject, lineValue.Split("\t")[index]);
                }
            }
        }

    }


}