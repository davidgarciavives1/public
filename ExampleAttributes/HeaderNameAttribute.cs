﻿using System;

[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
public class HeaderNameAttribute : Attribute
{
    public readonly string fieldName = "";

    public HeaderNameAttribute(string fieldName)
    {
        this.fieldName = fieldName;
    }
}