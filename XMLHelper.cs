﻿using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using DAL.Common;

namespace DAL.Common
{
    public class XMLHelper
    {
        public static XElement GetXML(string url)
        {


            try
            {
                StreamReader streamReader;
                HttpWebResponse response;
                HttpWebRequest detailsRequest = WebRequest.CreateHttp(url);

                detailsRequest.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(RobotsHelpers.GetCredentials())));
                detailsRequest.CookieContainer = CookieHelper.GlobalContainer;

                try
                {
					response = (HttpWebResponse)(detailsRequest.GetResponse());
                    if (response == null)
                    {
                        throw new Exception("No se ha recuperado una respuesta válida del servidor utilizando el código [" + url + "]");
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }

                //Recupero las cookies de salida de la petición XML
                CookieHelper.GlobalContainer = detailsRequest.CookieContainer;
				
                  if (response == null)
                {
                    throw new Exception("No se ha recuperado una respuesta válida del servidor utilizando el código [" + url + "]");
                }

                streamReader = new StreamReader(response.GetResponseStream(), Encoding.GetEncoding(response.CharacterSet));
                if (streamReader == null)
                {
                    throw new Exception("Error leyendo la respuesta del servidor para el código de búsqueda [" + url + "]");
                }


                // control de carácter raro que aparece en el response
                string s = streamReader.ReadToEnd();
				s = s.Replace("\u0010", "");
                s = s.Replace("\u001A", "");
                s = s.Replace("\u001B", "");
                s = s.Replace("\u001C", "");

                try
                {
                    XElement xe = XElement.Parse(s);
                    return xe;
                }
                catch (Exception e)
                {
                    throw new Exception("no se ha podido cargar el xml de los detalles descargados.", e);
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error recuperando XML en url " + url, e);
            }

        }

        public static string GetSingleValue(XElement rootNode, string rutaElemento, bool displayWarning=false)
        {
            //Posibilidad de recuperar el valor que está metido en el nodo raiz
            if( rutaElemento == "")
            {
                return rootNode.Value;
            }

            XElement myNode = rootNode.XPathSelectElement(rutaElemento);

            if (myNode == null)
            {
                if (displayWarning == true)
                {
                    RobotsHelpers.LogWarning("No se ha recuperado el elemento [" + rutaElemento + "]");
                }
                
                return null;
            }

            return myNode.Value;
        }

        

        public static IEnumerable<XElement> GetMultipleNodes(XElement rootNode, string path)
        {
            IEnumerable<XElement> ee = rootNode.XPathSelectElements(path);
            return ee;
        }

        public static XElement GetSingleNodes(XElement rootNode, string path)
        {
            if (path == "")
                return rootNode;

            return rootNode.XPathSelectElement(path);
        }


        /***********************************************************************************
         * *  Load properties with XMLPathAttribute, 
         * 
         * *    properties with XMLHeaderAttribute go in a different method 
         * ******************************************************************************************************/

        /// <summary>
        /// Función utilizada para asignar valores desde un fichero XML a las propiedades de una clase que implementa XMLPathAtribute
        /// Si las propiedades tienen el atributo XMLList además de XMLPathAttribute, entonces carga un nodo con múltiples valores en una colección del tipo que esté definido para la propiedad
        /// </summary>
        /// <param name="xe">XElement utilizado para cargar las propiedades de la clase</param>
        /// <param name="o">Objeto al que se le asignan los valores</param>
        /// <param name="clearNodes">Eliminar nodos que ya han sido asignados. Se utiliza en desarrollo para garantizar que todos los valores de un XML se están asignando</param>
        public static void LoadRegularFieldsFromXML(XElement xe, Object o, bool clearNodes=false )
        {
            PropertyInfo[] pi = o.GetType().GetProperties();

            foreach (PropertyInfo p in pi)
            {

                //Asignación de objetos a navigation properties
                if  ( p.CustomAttributes.Any(a => a.AttributeType == typeof(XMLListAttribute)) && p.CustomAttributes.Any(a => a.AttributeType == typeof(XMLPathAttribute)))
                {                    
                    // Credits to: https://archive.codeplex.com/?p=xef
                    
                    if (TryGetICollectionElementType(p.GetValue(o).GetType(), out Type childType))
                    {
                        //type of the elements in the collection is on childType
                        List<Object> myList = GetMultipleValues(xe, p.GetCustomAttribute<XMLPathAttribute>(true).XMLPath, childType, clearNodes);

                        //Clear all the relations
                        var clearMethod = p.GetValue(o).GetType().GetMethod("Clear");
                        clearMethod.Invoke( p.GetValue(o), null );

                        var addMethod = p.GetValue(o).GetType().GetMethod("Add", new[] { childType });
                        if (addMethod == null) return;
                        foreach (var childObject in myList)
                        {
                            addMethod.Invoke(p.GetValue(o), new[] { childObject });
                        }
                    }

                } /**************************   XMLPathAtribute goes here   ********************/
                //Asignación de propiedades a objetos
                else if (!p.GetMethod.IsVirtual && p.CustomAttributes.Any(a => a.AttributeType == typeof(XMLPathAttribute)))
                {

                    String myValue = XMLHelper.GetSingleValue(xe, p.GetCustomAttribute<XMLPathAttribute>(true).XMLPath);

                    if (p.PropertyType.ToString() == "System.String")
                    {
                        p.SetValue(o, myValue);
                    }
                    else if (p.PropertyType.ToString() == "System.Single")
                    {
                        p.SetValue(o, Single.Parse(myValue));
                    }
                    else if (p.PropertyType.ToString() == "System.Int32")
                    {
                        p.SetValue(o, int.Parse(myValue));
                    }
                    else if (p.PropertyType.ToString() == "System.DateTime")
                    {
                        if (DateTime.TryParseExact(myValue, "yyyyMMdd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out DateTime myDateTimeValue))
                        {
                            //myDateTimeValue is a datetime from a custom format parsed string
                            p.SetValue(o, myDateTimeValue);
                        }
                        else if (DateTime.TryParseExact(myValue, "yyyy-MM-dd-HH.mm.ss.ffffff", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out myDateTimeValue))
                        {
                            //myDateTimeValue is a datetime from a custom format parsed string

                            p.SetValue(o, myDateTimeValue);
                        }

                        else
                        {
                            //myValue is a string, default parsing
                            p.SetValue(o, DateTime.Parse(myValue));
                        }
                    }
                    else if (p.PropertyType.ToString() == "System.Nullable`1[System.DateTime]")
                    {
                        if (myValue == "" || myValue == null)
                        {
                            p.SetValue(o, null);
                        }
                        else
                        {
                            //Check for different formats for DateTime values
                            if (DateTime.TryParseExact(myValue, "yyyyMMdd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out DateTime myDateTimeValue))
                            {
                                //myDateTimeValue is a datetime from a custom format parsed string
                                p.SetValue(o, myDateTimeValue);
                            }
                            else if (DateTime.TryParseExact(myValue, "yyyy-MM-dd-HH.mm.ss.ffffff", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out myDateTimeValue))
                            {
                                //myDateTimeValue is a datetime from a custom format parsed string

                                p.SetValue(o, myDateTimeValue);
                            }
                            else if (DateTime.TryParseExact(myValue, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out myDateTimeValue))
                            {
                                //myDateTimeValue is a datetime from a custom format parsed string
                                p.SetValue(o, myDateTimeValue);
                            }

                            else
                            {
                                //myValue is a string, default parsing
                                p.SetValue(o, DateTime.Parse(myValue));
                            }

                        }
                    }
                    else if (p.PropertyType.ToString() == "System.Nullable`1[System.Single]")
                    {
                        if (myValue == "" || myValue == null)
                        {
                            p.SetValue(o, null);
                        }
                        else
                        {
                            p.SetValue(o, Single.Parse(myValue));
                        }
                    }
                    else if (p.PropertyType.ToString() == "System.Nullable`1[System.Int32]")
                    {
                        if (myValue == "" || myValue == null)
                        {
                            p.SetValue(o, null);
                        }
                        else
                        {
                            p.SetValue(o, System.Int32.Parse(myValue));
                        }
                    }
                    else
                    {
                        throw new Exception("Uncontrolled type: " + p.PropertyType.ToString());
                        //Uncontrolled type
                    }

                    // Default assign without type
                    // p.SetValue(o, XMLHelper.GetSingleValue(xe, p.GetCustomAttribute<XMLPathAttribute>(true).XMLPath));

                    //Eliminar el valor del xml después de asignarlo a una propiedad
                    if(clearNodes == true)
                    {
                        XElement xee = GetSingleNodes(xe, p.GetCustomAttribute<XMLPathAttribute>(true).XMLPath);
                        if (xee != null)
                            xee.Value = "";
                    }
                    
                }
            }
        }



        /****************************************** ********************************
         *  *  Load properties with XMLHeaderAttibute 
         *  *************************************************************************/
         /// <summary>
         /// Carga desde un xml el valor para las propiedades que tienen el atributo XMLHeader definido en la clase
         /// </summary>
         /// <param name="xe">XElement con el XML de los valores a recuperar</param>
         /// <param name="o">objeto donde se van a asignar los valores</param>
         /// <param name="clearNodes"></param>
        public static void LoadHeaderFieldsFromXML(XElement xe, Object o, bool clearNodes = false)
        {
            PropertyInfo[] pi = o.GetType().GetProperties();

            foreach (PropertyInfo p in pi)
            {

               if (!p.GetMethod.IsVirtual && p.CustomAttributes.Any(a => a.AttributeType == typeof(XMLHeaderAttribute)))
                {

                    String myValue = XMLHelper.GetSingleValue(xe, p.GetCustomAttribute<XMLHeaderAttribute>(true).XMLHeaderPath);

                    if (p.PropertyType.ToString() == "System.String")
                    {
                        p.SetValue(o, myValue);
                    }
                    else if (p.PropertyType.ToString() == "System.Single")
                    {
                        p.SetValue(o, Single.Parse(myValue));
                    }
                    else if (p.PropertyType.ToString() == "System.Int32")
                    {
                        p.SetValue(o, int.Parse(myValue));
                    }
                    else if (p.PropertyType.ToString() == "System.DateTime")
                    {
                        p.SetValue(o, DateTime.Parse(myValue));
                    }
                    else if (p.PropertyType.ToString() == "System.Nullable`1[System.DateTime]")
                    {
                        if (myValue == "" || myValue == null)
                        {
                            p.SetValue(o, null);
                        }
                        else
                        {
                            p.SetValue(o, DateTime.Parse(myValue));
                        }
                    }
                    else if (p.PropertyType.ToString() == "System.Nullable`1[System.Single]")
                    {
                        if (myValue == "" || myValue == null)
                        {
                            p.SetValue(o, null);
                        }
                        else
                        {
                            p.SetValue(o, Single.Parse(myValue));
                        }
                    }
                    else if (p.PropertyType.ToString() == "System.Nullable`1[System.Int32]")
                    {
                        if (myValue == "" || myValue == null)
                        {
                            p.SetValue(o, null);
                        }
                        else
                        {
                            p.SetValue(o, System.Int32.Parse(myValue));
                        }
                    }
                    else
                    {
                        throw new Exception("Uncontrolled type: " + p.PropertyType.ToString());
                        //Uncontrolled type
                    }

                    // Default assign without type
                    // p.SetValue(o, XMLHelper.GetSingleValue(xe, p.GetCustomAttribute<XMLPathAttribute>(true).XMLPath));

					//Eliminar el valor del xml después de asignarlo a una propiedad
                    if(clearNodes == true)
                    {
                        XElement xee = GetSingleNodes(xe, p.GetCustomAttribute<XMLHeaderAttribute>(true).XMLHeaderPath);
                        if (xee != null)
                            xee.Value = "";
                    }
                    																											 
                }
            }
        }


        //public static Collection<T> GetMultipleValues<T>(XElement xElement, string path)
        /// <summary>
        /// Devuelve una lista de elementos de un tipo que se le pasa como parámetro, inicializados con valores de un XML con varios nodos que también se le pasa como parámetro
        /// </summary>
        /// <param name="xElement">XML que contiene los valores</param>
        /// <param name="path">Ruta dentro del XML de los valores </param>
        /// <param name="myType">Tipo de los elementos de la colección o lista que devuelve</param>
        /// <param name="clearNodes"></param>
        /// <returns></returns>
        public static List<Object> GetMultipleValues(XElement xElement, string path, Type myType, bool clearNodes = false) 
        {            

            List<Object> myList = new List<object>();                

            foreach (XElement e in GetMultipleNodes(xElement, path ))
            {

                var instance = Activator.CreateInstance(myType);
                
                LoadRegularFieldsFromXML(e, instance, clearNodes);

                myList.Add(instance);
            }

            return myList;

        }

        /// <summary>
        /// Tries the type of the get I collection element.
        /// </summary>
        /// <param name="collectionType">Type of the collection.</param>
        /// <param name="elementType">Type of the element.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
        public static bool TryGetICollectionElementType(Type collectionType, out Type elementType)
        {
            elementType = null;
            // We have to check if the type actually is the interface, or if it implements the interface:
            try
            {
                var collectionInterface =
                    (collectionType.IsGenericType && typeof(ICollection<>).IsAssignableFrom(collectionType.GetGenericTypeDefinition()))
                        ? collectionType
                        : collectionType.GetInterface(typeof(ICollection<>).FullName);

                // We need to make sure the type is fully specified otherwise we won't be able to add element to it.
                if (collectionInterface != null
                    && !collectionInterface.ContainsGenericParameters)
                {
                    elementType = collectionInterface.GetGenericArguments()[0];
                    return true;
                }
            }
            catch (AmbiguousMatchException)
            {
                // Thrown if collection type implements ICollection<> more than once
            }
            return false;
        }


        public static bool NodoTieneValor(XElement e)
        {
            try
            {
                foreach (var e3 in e.Element("RESPUESTA").Descendants())
                {
                    if (e3.Value.Replace("\r\n", "").Replace("\n\r", "").Replace("\r", "").Replace("\n", "").Trim() != "")
                    {
                        //e3.Value = "Encontrado";
                        return true;
                    }

                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }
    }
}
