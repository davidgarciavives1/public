﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace RoadTech.DAL
{
    public static class Mapper<TEntity> where TEntity : class, new()
    {


        public static TEntity Map(DataRow tableRow) 
        {
            // Create a new type of the entity I want
            Type t = typeof(TEntity);
            TEntity returnObject = new TEntity();

            foreach (DataColumn col in tableRow.Table.Columns)
            {
                string colName = col.ColumnName;

                // Look for the object's property with the columns name, ignore case
                PropertyInfo pInfo = t.GetProperty(colName.ToLower(),
                    BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

                // did we find the property ?
                if (pInfo != null)
                {
                    object val = tableRow[colName];

                    // is this a Nullable<> type
                    bool IsNullable = (Nullable.GetUnderlyingType(pInfo.PropertyType) != null);
                    if (IsNullable)
                    {
                        if (val is System.DBNull)
                        {
                            val = null;
                        }
                        else
                        {
                            // Convert the db type into the T we have in our Nullable<T> type
                            val = Convert.ChangeType
           (val, Nullable.GetUnderlyingType(pInfo.PropertyType));
                        }
                    }
                    else
                    {
                        // Convert the db type into the type of the property in our entity
                        val = Convert.ChangeType(val, pInfo.PropertyType);
                    }
                    // Set the value of the property with the value from the db
                    pInfo.SetValue(returnObject, val, null);
                }
            }

            // return the entity object with values
            return returnObject;
        }


        /// <summary>
        /// Return an object for each row
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public static IEnumerable<TEntity> Map(DataTable table)
        {

            List<TEntity> entities = new List<TEntity>();
            foreach (DataRow row in table.Rows)
            {

                entities.Add(Map(row));
            }

            return entities;
        }



    }
}

