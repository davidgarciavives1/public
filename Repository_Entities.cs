﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

    public partial class Repository_Entities
    {
		private SqlCommand GetCmd(FormattableString sql)
		{
            var cmd = new SqlCommand() { CommandType = CommandType.Text };

			var parameters = sql.GetArguments().Select((e, i) =>
			{
				var p = cmd.CreateParameter();
				p.ParameterName = $"@p{i}";
				p.Value = e;
				return p;
			}).ToArray();

			cmd.Parameters.AddRange(parameters);
			cmd.CommandText = string.Format(sql.Format, parameters.Select(x => x.ParameterName).ToArray());

			return cmd;
		}



        /// <summary>
        ///     ''' Ejecuta una consulta SQL que recibe como parámetro y devuelve un datatable
        ///     ''' </summary>
        ///     ''' <param name="sSQL"></param>
        ///     ''' <returns>Resultado de ejecución de la consulta</returns>
        public  DataTable ExecuteDataTable(FormattableString sql)
        {
            return ExecuteDataTable(GetCmd(sql));
        }


        /// <summary>
        ///     ''' Ejecuta un comando que recibe como parámetro y devuelve varios registros
        ///     ''' </summary>
        ///     ''' <param name="cmd"></param>
        ///     ''' <returns>Devuelve varios registros</returns>
        public  DataTable ExecuteDataTable(SqlCommand cmd)
        {
            DataTable dt = new DataTable();
            using (SqlConnection cn = getSQLConnection())
            {
                cmd.Connection = cn;
                cn.Open();
                try
                {
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                        dr.Close();
                        return dt;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.ToString());
                }
            }
        }

        public  DataRow ExecuteDataRow(SqlCommand cmd)
        {
            DataTable dt = new DataTable();
            using (SqlConnection cn = getSQLConnection())
            {
                cmd.Connection = cn;
                cn.Open();
                try
                {
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            DataTable tabla = new DataTable();

                            DataTable schemaTable = dr.GetSchemaTable();
                            foreach (DataRow row in schemaTable.Rows)
                            {
                                string colName = (string)row["ColumnName"];
                                Type t = (Type)row["DataType"];
                                tabla.Columns.Add(colName, t);
                            }

                            dr.Read();

                            DataRow r = tabla.NewRow();
                            foreach (DataColumn column  in tabla.Columns)
                            {
                                r[column] = dr[column.ColumnName];
                            }

                            dr.Close();
                            return r;
                        }
                    }

                    return null;
                }
                catch (Exception ex)
                {
                    throw new Exception($"{System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name}", ex);
                }
            }
        }



        /// <summary>
        ///     ''' Ejecuta un comando que se le pasa como parámetro
        ///     ''' </summary>
        ///     ''' <param name="cmd">SQLCommand a ejecutar</param>
        ///     ''' <returns>Devuelve un único valor</returns>
        public  object ExecuteScalar(SqlCommand cmd)
        {
            using (SqlConnection cn = getSQLConnection())
            {
                cn.Open();
                cmd.Connection = cn;
                return cmd.ExecuteScalar();
            }
        }

        /// <summary>
        ///     ''' Ejecuta una SQL que se le pasa como parámetro
        ///     ''' </summary>
        ///     ''' <param name="sql">Sentencia SQL a ejecutar</param>
        ///     ''' <returns>Devuelve un único valor</returns>
        public  object ExecuteScalar(FormattableString sql)
        {
            using (SqlConnection cn = getSQLConnection())
            {
                using (SqlCommand cmd = GetCmd(sql))
                {
                    cmd.CommandType = CommandType.Text;
                    return ExecuteScalar(GetCmd(sql));
                }
            }
        }




        /// <summary>
        ///     ''' Ejecuta un comando SQL que se le pasa como parámetro
        ///     ''' </summary>
        ///     ''' <param name="text"></param>
        ///     ''' <returns>Número de filas afectadas</returns>
        public  int ExecuteNonQuery(FormattableString sql)
        {
            using (SqlCommand cmd = GetCmd(sql))
            {
                using (SqlConnection cn = getSQLConnection())
                {
                    cmd.Connection = cn;
                    cn.Open();
                    return cmd.ExecuteNonQuery();
                }
            }
        }


        /// <summary>
        ///     ''' Ejecuta un comando SQL que se le pasa como parámetro
        ///     ''' </summary>
        ///     ''' <param name="cmd">Comando a ejecutar</param>
        ///     ''' <returns>Número de filas afectadas</returns>
        public  int ExecuteNonQuery(SqlCommand cmd)
        {
            try
            {
                using (SqlConnection cn = getSQLConnection())
                {
                    cmd.Connection = cn;
                    cn.Open();
                    return cmd.ExecuteNonQuery();
                }
            }
            catch (Exception EX)
            {
                Console.WriteLine(EX.ToString());
                throw EX;
            }
        }

        /// <summary>
        ///     ''' Devuelve una conexión a la base de datos SQL que estemos usando
        ///     ''' </summary>
        ///     ''' <returns></returns>
        private  SqlConnection getSQLConnection()
        {
            return new SqlConnection(this.Database.Connection.ConnectionString);
        }


    }

